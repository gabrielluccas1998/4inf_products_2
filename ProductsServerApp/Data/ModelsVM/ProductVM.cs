﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProductsServerApp.Data.ModelsVM
{
    public class ProductVM
    {
        [Key]
        public int ProductID { get; set; }
        [Required, StringLength(50)]
        public string? Title { get; set; }
        [Required, Column(TypeName = "money")]
        public decimal Price { get; set; }
    }
}
