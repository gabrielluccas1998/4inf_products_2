﻿using Microsoft.EntityFrameworkCore;
using ProductsServerApp.Data.Models;

namespace ProductsServerApp.Data.Repos
{
    public class OrderRepo
    {
        private ApplicationDbContext _dbContext;
        //DB Context
        public OrderRepo(ApplicationDbContext dbContext)
        {
            this._dbContext = dbContext;
        }

        public async Task<Order> GetOrderByNr(int nr)
        {
            return await _dbContext.Orders.FirstOrDefaultAsync(x => x.OrderNr == nr);
        }

        //Read
        public async Task<List<Order>> GetAllOrdersAsync()
        {
            return await _dbContext.Orders
                .Include(x => x.User)
                .ToListAsync();
        }
        //Delete
        public async Task DelOrderAsync(Order order)
        {
            _dbContext.Orders.Remove(order);
        }
        //Create
        public async Task AddOrder(Order order)
        {
            await _dbContext.Orders.AddAsync(order);
        }
        //Update
        public async Task UpdateOrderAsync(Order order)
        {
            _dbContext.Orders.Update(order);
        }
    }
}
