﻿using Microsoft.EntityFrameworkCore;
using ProductsServerApp.Data.Models;
using ProductsServerApp.Data;

namespace ProductsServerApp.Data.Repos
{
    public class UserRepo
    {
        private ApplicationDbContext _dbContext;

        public UserRepo(ApplicationDbContext dbContext)
        {
            this._dbContext = dbContext;
        }

        public async Task<User> GetUserById(int id)
        {
            return await _dbContext.Users.FirstOrDefaultAsync(x => x.UserID == id);
        }

        public async Task DelUserAsync(int id)
        {
            var user = await GetUserById(id);

            if (user != null) _dbContext.Users.Remove(user);
        }

        public async Task<List<User>> GetAllUsersAsync()
        {
            return await _dbContext.Users.ToListAsync();
        }

        public async Task AddUser(User user)
        {
            await _dbContext.Users.AddAsync(user);
        }

        public void UpdateUser(User user)
        {
            _dbContext.Users.Update(user);
        }
    }
}
