﻿using Microsoft.EntityFrameworkCore;
using ProductsServerApp.Data.Models;
using ProductsServerApp.Data.ModelsVM;

namespace ProductsServerApp.Data.Repos
{
    public class ProductRepo
    {
        private ApplicationDbContext dbContext;

        public ProductRepo(ApplicationDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public async Task<Product> GetProductByIdAsync(int id)
        {
            return await dbContext.Products.FirstOrDefaultAsync(x => x.ProductID == id);
        }

        public async Task<List<ProductProperty>> GetProductPropFromProductByIdAsync(int productId)
        {
            var product = await GetProductByIdAsync(productId);
            return await dbContext.ProductProperties.Where(x => x.Products.Contains(product)).ToListAsync();
        }

        public async Task<List<ProductProperty>> GetFreeProductPropFromProductByIdAsync(int productId)
        {
            var product = await GetProductByIdAsync(productId);
            return await dbContext.ProductProperties.Where(x => !x.Products.Contains(product)).ToListAsync();
        }

        public async Task RemProductPropFromProductByIdAsync(int productId, int productPropertyId)
        {
            var productProperty = await dbContext.ProductProperties
                .FirstOrDefaultAsync(x => x.ProductPropertyID == productPropertyId);
            var product = await GetProductByIdAsync(productId);

            await dbContext.Entry(product).Collection(p => p.ProductProperties).LoadAsync();

            product.ProductProperties.Remove(productProperty);
            dbContext.Products.Attach(product);
            dbContext.Entry(product).State = EntityState.Modified;
        }

        public async Task<List<ProductVM>> GetProductVMsAsync()
        {
            var productsVM = from p in dbContext.Products
                             select new ProductVM
                             {
                                 ProductID = p.ProductID,
                                 Title = p.Title,
                                 Price = p.Price
                             };
            return await productsVM.ToListAsync();
        }

        public async Task AddProductPropToProduct(int productID, int productPropID)
        {
            var productProperty = await dbContext.ProductProperties.FirstOrDefaultAsync(x => x.ProductPropertyID == productPropID);
            var product = await dbContext.Products.FirstOrDefaultAsync(x => x.ProductID == productID);
            if(product.ProductProperties == null) product.ProductProperties = new List<ProductProperty>();
            product.ProductProperties.Add(productProperty);
            dbContext.Products.Attach(product);
            dbContext.Entry(product).State = EntityState.Modified;
        }
    }
}
