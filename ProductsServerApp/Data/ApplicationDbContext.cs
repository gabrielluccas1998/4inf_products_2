﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using ProductsServerApp.Data.Models;

namespace ProductsServerApp.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<ProductProperty> ProductProperties { get; set; }
        public virtual DbSet<Order> Orders { get; set; }
        public virtual DbSet<OrderPosition> OrderPositions { get; set; }
        public virtual DbSet<Product> Products { get; set; }
        public virtual DbSet<User> Users { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.Entity<OrderPosition>()
                .HasKey(x => new { x.PositionNr, x.OrderNr});

            builder.Entity<User>().HasData(
                new User { UserID = 1, FirstName = "Ralph", LastName = "Kartnaller", LoginName = "KaRR", Password = "1234"},
                new User { UserID = 2, FirstName = "Max", LastName = "Mustersupermann", LoginName = "MAXX", Password = "1234" }
                );
        }
    }
}