﻿using ProductsServerApp.Data.Repos;

namespace ProductsServerApp.Data
{
    public class UnitOfWork:IUnitOfWork,IDisposable
    {
        private ApplicationDbContext dbContext;
        private UserRepo userRepo;
        private OrderRepo orderRepo;
        private ProductRepo productRepo;

        public UnitOfWork(ApplicationDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public UserRepo UserRepo
        {
            get
            {
                if (userRepo == null) userRepo = new UserRepo(dbContext);
                return userRepo;
            }
        }

        public OrderRepo OrderRepo
        {
            get
            {
                if(orderRepo == null) orderRepo = new OrderRepo(dbContext);
                return orderRepo;
            }
        }

        public ProductRepo ProductRepo
        {
            get
            {
                if(productRepo == null) productRepo = new ProductRepo(dbContext);
                return productRepo;
            }
        }

        public async Task CommitAsync()
        {
            await dbContext.SaveChangesAsync();
        }

        public void Dispose()
        {
            dbContext.Dispose();
        }

    }
}
