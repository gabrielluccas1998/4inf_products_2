﻿using ProductsServerApp.Data.Models;
using ProductsServerApp.Data.Repos;

namespace ProductsServerApp.Data
{
    public interface IUnitOfWork
    {
        UserRepo UserRepo { get; }
        OrderRepo OrderRepo { get; }
        ProductRepo ProductRepo { get; }

        Task CommitAsync();
    }
}