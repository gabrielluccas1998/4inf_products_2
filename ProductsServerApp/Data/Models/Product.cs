﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductsServerApp.Data.Models
{
    public class Product
    {
        [Key]
        public int ProductID { get; set; }
        [Required, StringLength(50)]
        public string? Title { get; set; }
        [Required, Column(TypeName = "money")]
        public decimal Price { get; set; }
        public virtual ICollection<OrderPosition>? OrderPositions { get; set; }
        public virtual ICollection<ProductProperty>? ProductProperties { get; set; }
    }
}
