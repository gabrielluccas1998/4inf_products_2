﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductsServerApp.Data.Models
{
    public class Order
    {
        [Key]
        public int OrderNr { get; set; }
        [Required, DataType(DataType.DateTime)]
        public DateTime OrderDate { get; set; }
        [Required]
        public virtual User? User { get; set; }
        public virtual ICollection<OrderPosition>? OrderPositions { get; set; }
    }
}
