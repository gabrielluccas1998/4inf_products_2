﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductsServerApp.Data.Models
{
    public class User
    {
        [Key]
        public int UserID { get; set; }
        [StringLength(50)]
        public string? FirstName { get; set; }
        [Required, StringLength(50)]
        public string? LastName { get; set; }
        [Required, StringLength(100)]
        public string? LoginName { get; set; }
        [Required, StringLength(128), DataType(DataType.Password)]
        public string? Password { get; set; }
        [Required]
        public DateTime Created { get; set; } = DateTime.Now;
        [Required]
        public DateTime Expires { get; set; } = DateTime.Now.AddDays(30);
        public virtual ICollection<Order>? Orders { get; set; }

    }
}
