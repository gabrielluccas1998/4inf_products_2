﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductsServerApp.Data.Models
{
    public class OrderPosition
    {
        [ForeignKey("Order")]
        public int OrderNr { get; set; }
        public int PositionNr { get; set; }
        [Required]
        public int Amount { get; set; }
        [Required, Column(TypeName ="money")]
        public decimal Price { get; set; }
        [Required]
        public virtual Order? Order { get; set; } 
        [Required]
        public virtual Product? Product { get; set; }
    }
}
