﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductsServerApp.Data.Models
{
    public class ProductProperty
    {
        [Key]
        public int ProductPropertyID { get; set; }

        [Required, StringLength(50)]
        public string? Title { get; set; }

        [StringLength(250)]
        public string? Notes { get; set; }

        public virtual ICollection<Product>? Products { get; set; }
    }
}
