﻿using Microsoft.AspNetCore.Components;
using ProductsServerApp.Data.Models;
using System.Security.Cryptography;

namespace ProductsServerApp.Pages
{
    public partial class UserEdit
    {
        [Parameter]
        public string? UserId { get; set; }
        public User User { get; set; } = new User();
        private string? newPassword;
        private string? oldPassword;
        private string? passwordError;
        private bool verify(string password, string hashedPassword)
        {
            byte[] hashBytes = Convert.FromBase64String(hashedPassword);
            byte[] salt = new byte[16];
            Array.Copy(hashBytes, 0, salt, 0, 16);
            var pbkdf = new Rfc2898DeriveBytes(password, salt, 100000);
            byte[] hash = pbkdf.GetBytes(20);

            for(int i = 0; i<20; i++)
            {
                if (hashBytes[i + 16] != hash[i]) return false;
            }
            return true;
        }
        private string hashPassword(string pwd)
        {
            byte[] salt;
            new RNGCryptoServiceProvider().GetBytes(salt = new byte[16]);

            var pbkdf2 = new Rfc2898DeriveBytes(pwd, salt, 100000);
            byte[] hash = pbkdf2.GetBytes(20);

            byte[] hashBytes = new byte[36];
            Array.Copy(salt, 0, hashBytes, 0, 16);
            Array.Copy(hash, 0, hashBytes, 16, 20);

            return Convert.ToBase64String(hashBytes);
        }
        protected async Task OnValidSubmission()
        {
            if (UserId != null)
            {
                if(verify(oldPassword, User.Password))
                {
                    User.Password = hashPassword(newPassword);
                    uow.UserRepo.UpdateUser(User);
                    await uow.CommitAsync();
                }
                else
                {
                    passwordError = "Incorrect Password";
                    return;
                }
            }
            else
            {
                User.Password = hashPassword(User.Password);
                await uow.UserRepo.AddUser(User);
                await uow.CommitAsync();
            }
            NavManager.NavigateTo("/useroverview");
        }

        protected override async Task OnInitializedAsync()
        {
            if (UserId != null)
            {
                User = await uow.UserRepo.GetUserById(int.Parse(UserId));
            }
        }
    }
}
