﻿using Microsoft.AspNetCore.Components;
using ProductsServerApp.Data.Models;

namespace ProductsServerApp.Pages.Order_Pages
{
    public partial class OrderEdit
    {
        [Parameter]
        public string? OrderNr { get; set; }
        public Order Order { get; set; } = new Order();
        public User User { get; set; } = new User();
        public List<User>? Users { get; set; }
        protected override async Task OnInitializedAsync()
        {
            Users = await uow.UserRepo.GetAllUsersAsync();
        }
    }
}
